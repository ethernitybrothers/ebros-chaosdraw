
#include <SDL.h>
#include <ctime>
#include "ChaosCanvas.h"
#include "Engine.h"

SDL_Surface*    surf_display;

const int FRAMES_PER_SECOND = 20;

int chaos_main()
{
	Engine engine;
	if (engine.init() != 0)
		return -1;

	engine.run();

	engine.release();

	return 0;
}
