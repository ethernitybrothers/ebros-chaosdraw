#include "Engine.h"

#include <SDL_opengl.h>

#include <iostream>

static GLboolean should_rotate = GL_TRUE;

void Engine::setupOpenGL()
{
    float ratio = (float) width / (float) height;

    /* Our shading model--Gouraud (smooth). */
	glEnable(GL_TEXTURE_2D);
    glShadeModel( GL_SMOOTH );

    /* Culling. */
    glCullFace( GL_BACK );
    glFrontFace( GL_CCW );
    glEnable( GL_CULL_FACE );

    /* Set the clear color. */
    glClearColor( 0, 0, 0, 0 );

    /* Setup our viewport. */
    glViewport( 0, 0, width, height );

    /*
     * Change to the projection matrix and set
     * our viewing volume.
     */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    /*
     * EXERCISE:
     * Replace this with a call to glFrustum.
     */
    // gluPerspective( 60.0, ratio, 1.0, 1024.0 );
	glFrustum(-1, 1, -1, 1, 1, 1024);
}

Engine::Engine()
{
	was_init = false;
	display = NULL;
	chaos_canvas = NULL;
}

Engine::~Engine()
{
	if (display != NULL)
		delete display;

	if (was_init)
		SDL_Quit();
}

int Engine::init()
{
	if (!depth_scanner.init())
		return -1;

	if(SDL_Init(SDL_INIT_VIDEO) < 0)
        return -1;

	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

	width = 1024;
	height = 768;
	SDL_Surface *surf_display;
	if ((surf_display = SDL_SetVideoMode(width, height, 32, SDL_OPENGL|SDL_FULLSCREEN)) == NULL) {
		SDL_Quit();
        return -1;
	}
	was_init = true;

	setupOpenGL();

	display = new Surface(surf_display);
	chaos_canvas = new ChaosCanvas();

	chaos_canvas->start();
	chaos_canvas->init(*this);

	return 0;
}

void Engine::release()
{
	chaos_canvas->stop();
	depth_scanner.release();
}

void Engine::run()
{
	bool quit = false;
	SDL_Event event;
	SDL_ShowCursor(0);

	chaos_canvas->generateNoise();

	int textureScale = chaos_canvas->getTextureScale();

	while ( quit == false ) {
		while ( SDL_PollEvent( &event ) ) {
			//If the user has Xed out the window
			if (event.type == SDL_QUIT) {
				//Quit the program
				quit = true;
			}
			else if (event.type == SDL_KEYDOWN) {

				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
					quit = true;
					break;
				case SDLK_r:
					chaos_canvas->generateNoise();
					chaos_canvas->updateTexture();
					break;
				case SDLK_PAGEUP:
					textureScale ++;
					chaos_canvas->setTextureScale(textureScale);
					break;
				case SDLK_PAGEDOWN:
					if (textureScale > 1) {
						textureScale --;
						chaos_canvas->setTextureScale(textureScale);
					}
					break;
				case SDLK_1:
					chaos_canvas->showChaos();
					break;
				case SDLK_2:
					chaos_canvas->showDepthMap();
					break;
				}
			}
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		chaos_canvas->render(*this);
		SDL_GL_SwapBuffers();
	}
}
