#pragma once

#include <windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>

class Engine;

class GLEntity
{
public:
	void render(Engine &engine)
	{
	}

	void init(Engine &engine)
	{
	}

	void release(Engine &engine)
	{
	}

protected:
	void checkGLerror();
};