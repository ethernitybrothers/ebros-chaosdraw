#include "DepthScanner.h"

#include <iostream>

DepthScanner::DepthScanner() :
    nextDepthFrameEvent(INVALID_HANDLE_VALUE),
    depthStreamHandle(INVALID_HANDLE_VALUE),
    nuiSensor(NULL)
{
	mutex = SDL_CreateMutex();
	cond = SDL_CreateCond();
	running = false;
}

DepthScanner::~DepthScanner()
{
	SDL_DestroyMutex(mutex);
	SDL_DestroyCond(cond);
}

bool DepthScanner::init()
{
	INuiSensor * pNuiSensor;
    HRESULT hr;

    int iSensorCount = 0;
    hr = NuiGetSensorCount(&iSensorCount);
    if (FAILED(hr))
    {
		std::cout << "No Nui Sensors found" << std::endl;
        return false;
    }

    // Look at each Kinect sensor
    for (int i = 0; i < iSensorCount; ++i)
    {
        // Create the sensor so we can check status, if we can't create it, move on to the next
        hr = NuiCreateSensorByIndex(i, &pNuiSensor);
        if (FAILED(hr))
        {
            continue;
        }

        // Get the status of the sensor, and if connected, then we can initialize it
        hr = pNuiSensor->NuiStatus();
        if (S_OK == hr)
        {
            nuiSensor = pNuiSensor;
            break;
        }

        // This sensor wasn't OK, so release it since we're not using it
        pNuiSensor->Release();
    }

    if (NULL != nuiSensor)
    {
        // Initialize the Kinect and specify that we'll be using depth
        hr = nuiSensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH); 
        if (SUCCEEDED(hr))
        {
            // Create an event that will be signaled when depth data is available
            nextDepthFrameEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

            // Open a depth image stream to receive depth frames
            hr = nuiSensor->NuiImageStreamOpen(
                NUI_IMAGE_TYPE_DEPTH,
                NUI_IMAGE_RESOLUTION_640x480,
                0,
                2,
                nextDepthFrameEvent,
                &depthStreamHandle);
			width = 640;
			height = 480;
        }
    }

    if (NULL == nuiSensor || FAILED(hr))
    {
        std::cout << "No ready Kinect found" << std::endl;
        return false;
    }

	SDL_mutexP(mutex);
	running = true;
	SDL_mutexV(mutex);
	thread = SDL_CreateThread(thread_func, this);

	return true;
}


void DepthScanner::release()
{
	SDL_mutexP(mutex);
	running = false;
	SDL_CondSignal(cond);
	SDL_mutexV(mutex);
	SDL_WaitThread(thread,  NULL);

	nuiSensor->NuiShutdown();

	if (nextDepthFrameEvent != INVALID_HANDLE_VALUE)
    {
        CloseHandle(nextDepthFrameEvent);
    }

	SafeRelease(nuiSensor);
}


int DepthScanner::thread_func(void *param)
{
	DepthScanner *scanner = reinterpret_cast<DepthScanner*>(param);
	scanner->worker();
	return 0;
}


void DepthScanner::worker()
{

	const int eventCount = 1;
    HANDLE hEvents[eventCount];
	hEvents[0] = nextDepthFrameEvent;

    // Main message loop
    while (running)
    {

        // Check to see if we have either a message (by passing in QS_ALLINPUT)
        // Or a Kinect event (hEvents)
        // Update() will check for Kinect events individually, in case more than one are signalled
        DWORD dwEvent = MsgWaitForMultipleObjects(eventCount, hEvents, FALSE, INFINITE, QS_ALLINPUT);

        // Check if this is an event we're waiting on and not a timeout or message
        if (WAIT_OBJECT_0 == dwEvent)
        {
			if ( WAIT_OBJECT_0 == WaitForSingleObject(nextDepthFrameEvent, 0) )
			{
				processDepth();
			}
        }

		/*
        if (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE))
        {
        }
		*/
    }

}

void DepthScanner::processDepth()
{
	HRESULT hr;
    NUI_IMAGE_FRAME imageFrame;

    // Attempt to get the depth frame
    hr = nuiSensor->NuiImageStreamGetNextFrame(depthStreamHandle, 0, &imageFrame);
    if (FAILED(hr))
    {
        return;
    }

    INuiFrameTexture * pTexture = imageFrame.pFrameTexture;
    NUI_LOCKED_RECT LockedRect;

    // Lock the frame data so the Kinect knows not to modify it while we're reading it
    pTexture->LockRect(0, &LockedRect, NULL, 0);

	std::vector<unsigned short> frameData;
	frameData.resize(width*height);

    // Make sure we've received valid data
    if (LockedRect.Pitch != 0)
    {
        const USHORT * pBufferRun = (const USHORT *)LockedRect.pBits;
		int size = width*height;
        for (int i = 0; i < size; i++)
        {
            // discard the portion of the depth that contains only the player index
            frameData[i] = NuiDepthPixelToDepth(pBufferRun[i]);
        }
    }

    // We're done with the texture so unlock it
    pTexture->UnlockRect(0);

	SDL_mutexP(mutex);
	depthData.swap(frameData);
	SDL_CondSignal(cond);
	SDL_mutexV(mutex);

    // Release the frame
    nuiSensor->NuiImageStreamReleaseFrame(depthStreamHandle, &imageFrame);
}

void DepthScanner::getDepthBuffer(std::vector<unsigned short> &buffer)
{
	SDL_mutexP(mutex);
	buffer.resize(depthData.size());
	memcpy(buffer.data(), depthData.data(), sizeof(unsigned short)*depthData.size());
	SDL_mutexV(mutex);
}


