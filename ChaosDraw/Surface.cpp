#include "Surface.h"

#include <SDL_image.h>

#include <iostream>

static SDL_Surface * create_surface(int width, int height)
{
	#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    int rmask = 0xff000000;
    int gmask = 0x00ff0000;
    int bmask = 0x0000ff00;
    int amask = 0x000000ff;
#else
    int rmask = 0x000000ff;
    int gmask = 0x0000ff00;
    int bmask = 0x00ff0000;
    int amask = 0xff000000;
#endif
	return SDL_CreateRGBSurface(SDL_SWSURFACE|SDL_SRCALPHA, width, height, 32, rmask, gmask, bmask, amask);
}

Surface::Surface(SDL_Surface *surface)
{
	sdl_surface = surface;
}

Surface::Surface()
{
	SDL_Surface *video_surface = SDL_GetVideoSurface();
	sdl_surface = create_surface(video_surface->w, video_surface->h);
}


Surface::Surface(int width, int height)
{
	sdl_surface = create_surface(width, height);
}

Surface::Surface(const std::string &image)
{
	sdl_surface = IMG_Load(image.c_str());
	if (sdl_surface == NULL) {
		std::cerr << "Failed to load image " << image << ", error: " << IMG_GetError();
		exit(0);
	}
}

Surface::~Surface()
{
	SDL_FreeSurface(sdl_surface);
}

void Surface::shiftDown()
{
	Uint32 *pixels = getPixels();
	int pitch = getPitch();
	int height = getHeight();
	memcpy(pixels+pitch, pixels, 4*pitch*(height-1));
}

void Surface::shiftUp()
{
	Uint32 *pixels = getPixels();
	int pitch = getPitch();
	int height = getHeight();
	memcpy(pixels, pixels+pitch, 4*pitch*(height-1));
}

void Surface::shiftLeft()
{
	Uint32 *pixels = getPixels();
	int pitch = getPitch();
	int height = getHeight();
	memcpy(pixels, pixels+1, 4*pitch*height-1);
}

void Surface::shiftRight()
{
	Uint32 *pixels = getPixels();
	int pitch = getPitch();
	int height = getHeight();
	memcpy(pixels+1, pixels, 4*pitch*height-1);
}
