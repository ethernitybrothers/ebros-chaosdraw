#pragma once

#include <SDL.h>
#include <vector>
#include <list>

class RandomnessGenerator
{
public:
	RandomnessGenerator(void);
	~RandomnessGenerator(void);

	void start();
	void stop();

	void getValues(std::vector<Uint8> &block, unsigned size);

private:
	std::vector<Uint8> random_numbers;
	std::list<SDL_Thread*> threads;
	SDL_mutex *mutex;
	SDL_cond *cond;
	bool running;

	static int thread_func(void *param);

	void worker();
};

