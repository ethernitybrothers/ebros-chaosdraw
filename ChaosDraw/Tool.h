#pragma once

#include "Surface.h"

#include <vector>

class Tool
{
public:
	Tool(void);
	~Tool(void);

	virtual Surface * getCursor() =0;

	virtual void apply(Surface *surface, int x, int y) = 0;

protected:
	
	struct Coord
	{
		double mapx;
		double mapy;
	};

	struct TransformMap
	{
		std::vector<Coord> coords;
		int size;
	};

	void transform(Surface *surface, int x, int y, const TransformMap &map);

private:
	std::vector<Uint32> buffer;
};

