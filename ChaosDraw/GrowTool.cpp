#include "GrowTool.h"


GrowTool::GrowTool(int size)
{
	cursor = new Surface("cursor-growtool.png");
	
	mapxy.size = size;
	for (int y = 0; y < size; y++)
		for (int x = 0; x < size; x++) {
			Coord coord;
			coord.mapx = -1;
			coord.mapy = 2;
			mapxy.coords.push_back(coord);
		}
}

GrowTool::~GrowTool(void)
{
	delete cursor;
}

Surface * GrowTool::getCursor()
{
	return cursor;
}

void GrowTool::apply(Surface *surface, int x, int y)
{
	transform(surface, x, y, mapxy);
}
