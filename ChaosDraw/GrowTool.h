#pragma once

#include "Tool.h"



class GrowTool :
	public Tool
{
public:
	GrowTool(int size);
	~GrowTool(void);

	virtual Surface * getCursor();

	virtual void apply(Surface *surface, int x, int y);



private:
	Surface * cursor;

	TransformMap mapxy;
};

