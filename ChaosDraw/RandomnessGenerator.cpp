#include "RandomnessGenerator.h"

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>

#define MAX_THREADS 1
#define MIN_RANDOM_NUMBERS_AHEAD (1024*768)
#define RANDOM_NUMBERS_IN_BLOCK 2048

RandomnessGenerator::RandomnessGenerator(void)
{
	mutex = SDL_CreateMutex();
	cond = SDL_CreateCond();
	running = false;
}

RandomnessGenerator::~RandomnessGenerator(void)
{
	SDL_DestroyMutex(mutex);
	SDL_DestroyCond(cond);
}

int RandomnessGenerator::thread_func(void *param)
{
	RandomnessGenerator *generator = reinterpret_cast<RandomnessGenerator*>(param);
	generator->worker();
	return 0;
}

void RandomnessGenerator::start()
{
	SDL_mutexP(mutex);
	running = true;
	SDL_mutexV(mutex);
	for (int i = 0; i < MAX_THREADS; i++) {
		SDL_Thread *thread = SDL_CreateThread(thread_func, this);
		threads.push_back(thread);
	}
}

void RandomnessGenerator::stop()
{
	SDL_mutexP(mutex);
	running = false;
	SDL_CondBroadcast(cond);
	SDL_mutexV(mutex);
	for (std::list<SDL_Thread*>::iterator it = threads.begin(); it != threads.end(); it++) {
		SDL_WaitThread(*it,  NULL);
	}
}

void RandomnessGenerator::worker()
{
	std::vector<Uint8> block(RANDOM_NUMBERS_IN_BLOCK, 0);
	boost::random::mt19937 rng;     // produces randomness out of thin air
                                    // see pseudo-random number generators
	boost::random::uniform_int_distribution<Uint8> random_grey_scale(0,255);


	for (;;) {
		SDL_mutexP(mutex);
		while (random_numbers.size() >= MIN_RANDOM_NUMBERS_AHEAD && running)
			SDL_CondWait(cond, mutex);

		if (!running) {
			SDL_mutexV(mutex);
			return;
		}
		SDL_mutexV(mutex);

		for (int i = 0; i < RANDOM_NUMBERS_IN_BLOCK; i++) {		
			Uint8 value = (Uint8) random_grey_scale(rng);
			block[i] = value;
		}

		SDL_mutexP(mutex);
		random_numbers.insert(random_numbers.end(), block.begin(), block.end());
		SDL_CondBroadcast(cond);
		SDL_mutexV(mutex);
	}

}

void RandomnessGenerator::getValues(std::vector<Uint8> &block, unsigned size)
{
	block.clear();
	block.reserve(size);
	SDL_mutexP(mutex);
	while (random_numbers.size() < size)
			SDL_CondWait(cond, mutex);

	block.insert(block.begin(), random_numbers.end()-size, random_numbers.end());
	random_numbers.erase(random_numbers.begin(), random_numbers.begin()+size);
	SDL_CondBroadcast(cond);
	SDL_mutexV(mutex);
}
