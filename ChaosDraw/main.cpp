#include "chaos.h"

#include <stdio.h>  /* defines FILENAME_MAX */
#include <direct.h>
#include <cerrno>

#define GetCurrentDir _getcwd


 char cCurrentPath[FILENAME_MAX];



// this is here, because the VSC++ cannot handle SDL include in the main.cpp file
int main()
{
	 if (!GetCurrentDir(cCurrentPath, sizeof(cCurrentPath) / sizeof(char)))
     {
		return errno;
     }

	cCurrentPath[sizeof(cCurrentPath) - 1] = '\0'; /* not really required */

	printf ("The current working directory is %s", cCurrentPath);
	return chaos_main();
}
