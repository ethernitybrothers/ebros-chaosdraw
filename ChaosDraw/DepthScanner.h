#pragma once

#include <Windows.h>
#include <SDL.h>
#include "NuiApi.h"

#include <vector>

class DepthScanner
{
public:
	DepthScanner();
	~DepthScanner();

	bool init();
	void release();

	int getWidth() {
		return width;
	}

	int getHeight() {
		return height;
	}

	void getDepthBuffer(std::vector<unsigned short> &buffer);

private:
	// Current Kinect
    INuiSensor* nuiSensor;
	HANDLE depthStreamHandle;
    HANDLE nextDepthFrameEvent;
	std::vector<unsigned short> depthData;

	int width;
	int height;

	SDL_Thread *thread;
	SDL_mutex *mutex;
	SDL_cond *cond;
	bool running;

	static int thread_func(void *param);
	void worker();

	void processDepth();


	template<class Interface>
	void SafeRelease( Interface *& pInterfaceToRelease )
	{
		if ( pInterfaceToRelease != NULL )
		{
			pInterfaceToRelease->Release();
			pInterfaceToRelease = NULL;
		}
	}

};

