#pragma once

#include "RandomnessGenerator.h"
#include "GLEntity.h"

#include <vector>


class ChaosCanvas : public GLEntity
{
public:
	enum DrawType {
		DrawDepthMap,
		DrawChaos
	};
	ChaosCanvas();
	~ChaosCanvas();

	void start();
	void stop();

	void generateNoise();
	void generateNoiseAtRow(int row);
	void generateNoiseAtColumn(int column);

	void init(Engine &engine);
	void render(Engine &engine);
	void release(Engine &engine);

	void updateTexture() {
		doUpdateTexture = true;
	}

	void setTextureScale(int scale) {
		textureScale = scale;
	}

	int getTextureScale() const {
		return textureScale;
	}

	void showDepthMap() {
		drawType = DrawDepthMap;
	}

	void showChaos() {
		drawType = DrawChaos;
	}

private:
	RandomnessGenerator randomness_generator;
	GLuint texture;
	GLuint depthTexture;
	Uint32 *pixels;
	std::vector<float> zmap;
	std::vector<float> tmpzmap;
	std::vector<int> tmpzmap_npixels;
	std::vector<unsigned short> depthBuffer;
	std::vector<unsigned char> depth_texture_data;
	int width;
	int height;
	int blocksx;
	int blocksy;
	int zmap_width;
	int zmap_height;
	unsigned short minDepth;
	unsigned short maxDepth;
	int textureScale;
	bool doUpdateTexture;
	DrawType drawType;

	void updateZMap(Engine &engine);
	void renderChaos(Engine &engine);
	void renderDepthMap(Engine &engine);

	float z(int x, int y ) {
		return zmap[x + y*zmap_width];
	}
};
