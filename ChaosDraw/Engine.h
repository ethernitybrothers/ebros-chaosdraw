#pragma once

#include <SDL.h>
#include "Surface.h"
#include "ChaosCanvas.h"
#include "DepthScanner.h"

class Engine
{
public:
	Engine();
	~Engine();

	int init();
	void release();

	void run();

	int getScreenWidth() {
		return width;
	}
	int getScreenHeight() {
		return height;
	}

	DepthScanner & getDepthScanner() {
		return depth_scanner;
	}

private:
	bool was_init;
	Surface *display;
	ChaosCanvas *chaos_canvas;
	DepthScanner depth_scanner;
	int width;
	int height;

	void setupOpenGL();
};
