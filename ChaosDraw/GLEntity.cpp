#include "GLEntity.h"

#include <iostream>

void GLEntity::checkGLerror()
{
	GLenum errCode;
	const GLubyte *errString;
	if ((errCode = glGetError()) != GL_NO_ERROR) {
		errString = gluErrorString(errCode);
		std::cout << "OpenGL Error: " <<  errString << std::endl;
	}
}