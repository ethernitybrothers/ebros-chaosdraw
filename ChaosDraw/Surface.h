#pragma once

#include <SDL.h>
#include <string>
#include <stdexcept>

#include "Rect.h"

class Surface
{
public:
	Surface();
	Surface(const std::string &image);
	Surface(int width, int height);
	Surface(SDL_Surface *surface);
	~Surface();

	void blit(Rect &src_rect, Surface *dst, int x, int y)
	{
		Rect dst_rect;
		dst_rect.x = x;
		dst_rect.y = y;
		SDL_BlitSurface(sdl_surface, &src_rect, dst->sdl_surface, &dst_rect);
	}

	void blit(Surface *dst, int x, int y)
	{
		Rect dst_rect;
		dst_rect.x = x;
		dst_rect.y = y;
		SDL_BlitSurface(sdl_surface, NULL, dst->sdl_surface, &dst_rect);
	}

	void blit(Surface *dst)
	{
		SDL_BlitSurface(sdl_surface, NULL, dst->sdl_surface, NULL);
	}

	int getWidth() const
	{
		return sdl_surface->w;
	}

	int getHeight() const
	{
		return sdl_surface->h;
	}

	int getPitch() const
	{
		return sdl_surface->pitch / 4;
	}

	Uint32* getPixels()
	{
		return (Uint32*) sdl_surface->pixels;
	}

	void update()
	{
		SDL_UpdateRect(sdl_surface, 0, 0, sdl_surface->w, sdl_surface->h);
	}

	void flip()
	{
		SDL_Flip(sdl_surface);
	}

	void shiftDown();
	void shiftUp();
	void shiftLeft();
	void shiftRight();

private:
	SDL_Surface *sdl_surface;
};
