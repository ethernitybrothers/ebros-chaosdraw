#include "Tool.h"


Tool::Tool(void)
{
}


Tool::~Tool(void)
{
}

static inline int clamp(int value, int max)
{
	if (value < 0)
		return 0;
	if (value >= max)
		return max-1;
	return value;
}

static inline Uint32 bilinear_filter(Uint32 *pixels, int width, int height, int pitch, double x, double y)
{
	double xfloor = floor(x);
	double xceil = ceil(x);
	double yfloor = floor(y);
	double yceil = ceil(y);

	double xratio1 = x - xfloor;
	double yratio1 = y - yfloor;
	double xratio2 = 1 - xratio1;
	double yratio2 = 1 - yratio1;

	int clxfloor = clamp(xfloor, width);
	int clxceil = clamp(xceil, width);
	int clyfloor = clamp(yfloor, height);
	int clyceil = clamp(yceil, height);

	double ctl = pixels[clxfloor + pitch * clyfloor] & 0xff;
	double ctr = pixels[clxceil + pitch * clyfloor] & 0xff;
	double cbl = pixels[clxfloor + pitch * clyceil] & 0xff;
	double cbr = pixels[clxceil + pitch * clyceil] & 0xff;

	double vt = ctl * xratio1 + ctr * xratio2;
	double vb = cbl * xratio1 + cbr * xratio2;
	double v = vt * yratio1 + vb * yratio2;
	Uint8 b = (Uint8) v;

	return 0xff000000 | b << 16 | b << 8 | b;
}

void Tool::transform(Surface *surface, int centerx, int centery, const Tool::TransformMap &map)
{
	buffer.reserve(map.size*map.size);
	buffer.insert(buffer.begin(), map.size*map.size, 0);
	Uint32 *pixels = surface->getPixels();
	int half_size = map.size/2;
	int surface_width = surface->getWidth();
	int surface_height = surface->getHeight();
	int surface_pitch = surface->getPitch();
	for (int y = 0; y < map.size; y++) {
		int surface_y = y + centery - half_size;
		if (surface_y < 0 || surface_y >= surface_height)
			continue;
		for (int x = 0; x < map.size; x++) {
			int surface_x = x + centerx - half_size;
			if (surface_x < 0 || surface_x >= surface_width)
				continue;
			const Coord &coord = map.coords[x+y*map.size];
			Uint32 value = bilinear_filter(pixels, surface_width, surface_height, surface_pitch, surface_x+coord.mapx, surface_y+coord.mapy);
			buffer[x + y*map.size] = value;
		}
	}
	for (int y = 0; y < map.size; y++) {
		int surface_y = y + centery - half_size;
		if (surface_y < 0 || surface_y >= surface_height)
			continue;
		for (int x = 0; x < map.size; x++) {
			int surface_x = x + centerx - half_size;
			if (surface_x < 0 || surface_x >= surface_width)
				continue;
			pixels[surface_x + surface_y*surface_pitch] = buffer[x + y*map.size];
		}
	}
}
