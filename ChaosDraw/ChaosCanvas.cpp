#include "ChaosCanvas.h"
#include "Engine.h"
#include <cassert>
#include <random>
#include <iostream>

#define BLOCK_SIZE 8

ChaosCanvas::ChaosCanvas()
{
	minDepth = 100;
	maxDepth = 3000;
	textureScale = 1;
	doUpdateTexture = true;
	drawType = DrawDepthMap;
}


ChaosCanvas::~ChaosCanvas()
{

}

void ChaosCanvas::start()
{
	randomness_generator.start();
}

void ChaosCanvas::stop()
{
	randomness_generator.stop();
}


void ChaosCanvas::generateNoise()
{
	for (int y = 0; y < height; y++)
		generateNoiseAtRow(y);
}

void ChaosCanvas::generateNoiseAtRow(int row)
{
	std::vector<Uint8> random_values;
	randomness_generator.getValues(random_values, width);
	for (int x = 0; x < width; x++) {		
		Uint8 value = random_values[x];
		Uint32 hval = 0xff000000 | value << 8 | value << 16 | value;
		pixels[x + row * width] = hval;
	}
}

void ChaosCanvas::generateNoiseAtColumn(int column)
{
	std::vector<Uint8> random_values;
	randomness_generator.getValues(random_values, height);
	for (int y = 0; y < height; y++) {		
		Uint8 value = random_values[y];
		Uint32 hval = 0xff000000 | value << 8 | value << 16 | value;
		pixels[column + y * width] = hval;
	}
}

void ChaosCanvas::init(Engine &engine)
{
	width = engine.getScreenWidth();
	height = engine.getScreenHeight();
	pixels = (Uint32*) malloc(sizeof(Uint32)*width*height);
	generateNoise();
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

	glGenTextures(1, &depthTexture);

	blocksx = width / BLOCK_SIZE;
	blocksy = height / BLOCK_SIZE;
	zmap_width =  blocksx + 1;
	zmap_height = blocksy + 1;
	zmap.resize(zmap_width*zmap_height);
	tmpzmap.resize(zmap_width*zmap_height);
	tmpzmap_npixels.resize(zmap_width*zmap_height);
}

void ChaosCanvas::render(Engine &engine)
{
	switch (drawType) {
	case DrawDepthMap:
		renderDepthMap(engine);
		break;
	case DrawChaos:
		renderChaos(engine);
		break;
	}

}

void ChaosCanvas::renderDepthMap(Engine &engine)
{
	DepthScanner &depth_scanner = engine.getDepthScanner();
	depth_scanner.getDepthBuffer(depthBuffer);

	int dw = depth_scanner.getWidth();
	int dh = depth_scanner.getHeight();
	depth_texture_data.resize(dw * dh * 4);
	for (int y = 0; y < dh; y++) {
		for (int x = 0; x < dw; x++) {
			unsigned short d = depthBuffer[x + (dh - y - 1)*dw];
			/*
			if (d > maxDepth)
				d = maxDepth;
				*/
			unsigned char c = (unsigned char) d % 256; // ((255 * (int) d) / maxDepth);
			int base = (x + y * dw) * 4;
			depth_texture_data[base + 0] = c;
			depth_texture_data[base + 1] = c;
			depth_texture_data[base + 2] = c;
			depth_texture_data[base + 3] = 255;
		}
	}

	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, dw, dh, 0, GL_RGBA, GL_UNSIGNED_BYTE, depth_texture_data.data());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

	int w = engine.getScreenWidth();
	int h = engine.getScreenHeight();

	/* We don't want to modify the projection matrix. */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

	glScalef(2.0/w, 2.0/h, 1);
	glTranslatef(-w/2, -h/2, 0);

	float z = -1.1;
		
	glBegin( GL_QUADS );
	glTexCoord2f(0, 0);
	glVertex3f(0, 0, z);
	glTexCoord2f(1, 0);
	glVertex3f(BLOCK_SIZE * blocksx, 0, z);
	glTexCoord2f(1, 1);
	glVertex3f(BLOCK_SIZE * blocksx, BLOCK_SIZE * blocksy, z);
	glTexCoord2f(0, 1);
	glVertex3f(0, BLOCK_SIZE * blocksy, z);
	glEnd();
}

void ChaosCanvas::renderChaos(Engine &engine)
{
	updateZMap(engine);

	glBindTexture(GL_TEXTURE_2D, texture);
	if (doUpdateTexture) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		doUpdateTexture = false;
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

	int w = engine.getScreenWidth();
	int h = engine.getScreenHeight();

	/* We don't want to modify the projection matrix. */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

	glScalef(2.0/w, 2.0/h, 1);
	glTranslatef(-w/2, -h/2, 0);
		
   /* Send our triangle data to the pipeline. */
	glBegin( GL_QUADS );

	float ty2 = 0;
	for (int y = 0; y < blocksy; y++) {
		float ty1 = (float) (y + 1) / (textureScale * zmap_height);
		float tx2 = 0;
		for (int x = 0; x < blocksx; x++) {
			float tx1 = (float) (x + 1) / (textureScale * zmap_width);

			float zx1y1 = z(x, y);
			float zx1y2 = z(x, y+1);
			float zx2y1 = z(x+1, y);
			float zx2y2 = z(x+1, y+1);
			int vx1 = x * BLOCK_SIZE;
			int vx2 = (x + 1) * BLOCK_SIZE;
			int vy1 = y * BLOCK_SIZE;
			int vy2 = (y + 1) * BLOCK_SIZE;
			glTexCoord2f(tx1, ty1);
			glVertex3f(vx1, vy1, zx1y1);
			glTexCoord2f(tx2, ty1);
			glVertex3f(vx2, vy1, zx2y1);
			glTexCoord2f(tx2, ty2);
			glVertex3f(vx2, vy2, zx2y2);
			glTexCoord2f(tx1, ty2);
			glVertex3f(vx1, vy2, zx1y2);

			tx2 = tx1;
		}
		ty2 = ty1;
	}
    glEnd( );
}

void ChaosCanvas::release(Engine &engine)
{
	glDeleteTextures(1, &texture);
	glDeleteTextures(1, &depthTexture);
	free(pixels);
}

void ChaosCanvas::updateZMap(Engine &engine)
{
	DepthScanner &depth_scanner = engine.getDepthScanner();
	depth_scanner.getDepthBuffer(depthBuffer);
	for (int i = 0; i < tmpzmap.size(); i++) {
		tmpzmap[i] = 0;
		tmpzmap_npixels[i] = 0;
	}
	int dw = depth_scanner.getWidth();
	int dh = depth_scanner.getHeight();
	for (int y = 0; y < dh; y++) {
		for (int x = 0; x < dw; x++) {
			unsigned short d = depthBuffer[x + (dh - y - 1)*dw];
			if (d > maxDepth)
				d = maxDepth;
			float blockindex = ((float) blocksx * x) / dw;
			float blockindey = ((float) blocksy * y) / dh;
			int index = (int) blockindex + blocksx * (int) blockindey;
			if (tmpzmap_npixels[index] == 0) {
				tmpzmap[index] += d;
				tmpzmap_npixels[index] += 1;
			}
		}
	}
	for (int i = 0; i < zmap.size(); i++) {
		zmap[i] = -1.1 - 0.2 * (tmpzmap[i] / tmpzmap_npixels[i]) / maxDepth;
	}

	/*
	for (int y = 0; y < zmap_height; y++) {
		for (int x = 0; x < zmap_width; x++) {
			zmap[x + y * zmap_width] = -1.1 - (rand() % 100) / 400.0 ;
		}
	}
	std::cout << "min = " << min << " max = " << max << std::endl;
	*/

}
